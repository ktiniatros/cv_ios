//
//  FirstViewController.m
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import "AboutViewController.h"
#import "CVNetworkManager.h"
#import "CVItem.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "CVTableViewCell.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

NSMutableArray *items;

- (void)viewDidLoad {
    [super viewDidLoad];
    items = [[NSMutableArray alloc] initWithArray:@[]];
    _aboutTableView.rowHeight = UITableViewAutomaticDimension;
    _aboutTableView.estimatedRowHeight = 80;
    _experienceTableView.rowHeight = UITableViewAutomaticDimension;
    _experienceTableView.estimatedRowHeight = 80;
    _portfolioTableView.rowHeight = UITableViewAutomaticDimension;
    _portfolioTableView.estimatedRowHeight = 80;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)refresh{
    NSLog(@"exw: %d",self.tabBarController.selectedIndex);
    switch(self.tabBarController.selectedIndex){
        case 1:
        {
            [CVNetworkManager getExperienceItemsAnd:^(NSArray *newItems) {
                [items setArray:newItems];
                [_experienceTableView reloadData];
            }];
            break;
        }
        case 2:
        {
            [CVNetworkManager getPortfolioItemsAnd:^(NSArray *newItems) {
                [items setArray:newItems];
                [_portfolioTableView reloadData];
            }];
            break;
        }
        default:
            [CVNetworkManager getAboutItemsAnd:^(NSArray *newItems) {
                [items setArray:newItems];
                [_aboutTableView reloadData];
            }];
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CVTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        [tableView registerNib:[UINib nibWithNibName:@"Cell" bundle:nil] forCellReuseIdentifier:@"cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    }
    CVItem *item = [items objectAtIndex:indexPath.row];
    NSString *imagePath = item.imagePath;
    
    if(imagePath == nil){
        [cell.icon.constraints enumerateObjectsUsingBlock:^(__kindof NSLayoutConstraint * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.constant = 0.0f;
        }];
    }else{
        [cell.icon sd_setImageWithURL:[NSURL URLWithString:item.imagePath]];
    }
    
    cell.nameLabel.text = item.title;
    cell.textLabel.text = item.text;
    
    return cell;
}

@end
