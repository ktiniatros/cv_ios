//
//  CVItem.h
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVItem : NSObject

@property NSString *title;
@property NSString *text;
@property NSString *imagePath;

@end
