//
//  main.m
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
