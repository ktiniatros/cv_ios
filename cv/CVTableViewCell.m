//
//  CVTableViewCell.m
//  cv
//
//  Created by giorgos on 03/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import "CVTableViewCell.h"

@implementation CVTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _container.layer.cornerRadius = 10.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
