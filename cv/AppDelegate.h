//
//  AppDelegate.h
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

