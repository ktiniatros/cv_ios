//
//  CVNetworkManager.m
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import "CVNetworkManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "CVItem.h"

@implementation CVNetworkManager

NSString *baseURL = @"http://www.giorgos.nl/apps/me";

+ (void)getRequestFrom:(NSString *)url cb:(void (^)(NSArray*))cb{
    AFHTTPRequestOperationManager *m = [AFHTTPRequestOperationManager manager];
    m.requestSerializer = [AFJSONRequestSerializer serializer];
    m.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [m GET:url parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSArray *items = responseObject;
        NSMutableArray *cvItems = [[NSMutableArray alloc] init];
        [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CVItem *item = [[CVItem alloc] init];
            if([obj objectForKey:@"title"] != nil){
                item.title = [obj objectForKey:@"title"];
            }
            if([obj objectForKey:@"name"] != nil){
                item.title = [obj objectForKey:@"name"];
            }
            if([obj objectForKey:@"text"] != nil){
                item.text = [obj objectForKey:@"text"];
            }
            if([obj objectForKey:@"icon"] != nil){
                item.imagePath = [obj objectForKey:@"icon"];
            }
            [cvItems addObject:item];
        }];
        cb(cvItems);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        cb(@[]);
    }];
}

+ (void)getAboutItemsAnd:(void (^)(NSArray*))cb{
    [self getRequestFrom:[NSString stringWithFormat:@"%@%@", baseURL, @"/about.json"] cb:^(NSArray *items) {
        cb(items);
    }];
}

+ (void)getExperienceItemsAnd:(void (^)(NSArray*))cb{
    [self getRequestFrom:[NSString stringWithFormat:@"%@%@", baseURL, @"/experience.json"] cb:^(NSArray *items) {
        cb(items);
    }];
}

+ (void)getPortfolioItemsAnd:(void (^)(NSArray*))cb{
    [self getRequestFrom:[NSString stringWithFormat:@"%@%@", baseURL, @"/apps_ios.json"] cb:^(NSArray *items) {
        cb(items);
    }];
}

@end
