//
//  CVNetworkManager.h
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVNetworkManager : NSObject

+ (void)getAboutItemsAnd:(void (^)(NSArray*))cb;
+ (void)getExperienceItemsAnd:(void (^)(NSArray*))cb;
+ (void)getPortfolioItemsAnd:(void (^)(NSArray*))cb;

@end
