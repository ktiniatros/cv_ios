//
//  FirstViewController.h
//  cv
//
//  Created by giorgos on 02/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *aboutTableView;
@property (weak, nonatomic) IBOutlet UITableView *experienceTableView;
@property (weak, nonatomic) IBOutlet UITableView *portfolioTableView;


@end

