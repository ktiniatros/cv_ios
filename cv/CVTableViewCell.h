//
//  CVTableViewCell.h
//  cv
//
//  Created by giorgos on 03/08/16.
//  Copyright © 2016 giorgos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
